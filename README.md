# A collection of scripts for generating a timelapse of your base in Factorio

To generate a timelapse video the workflow looks as follows
```
> $ ./make-symlinks.py /mnt/etc/factorio-app/raw/Bild_Mk1 /mnt/etc/factorio-app/preprocess/Mk1
Creating symlinks for project Bild_Mk1
Linking from /mnt/etc/factorio-app/raw/Bild_Mk1 to /mnt/etc/factorio-app/preprocess/Mk1

> $ ./generate-video.sh "/mnt/etc/factorio-app/preprocess/Mk2" /mnt/etc/factorio-app/output
ffmpeg version 4.3.2-0+deb11u1 Copyright (c) 2000-2021 the FFmpeg developers
  built with gcc 10 (Debian 10.2.1-6)
  configuration: --prefix=/usr --extra-version=0+deb11u1 --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu --arch=amd64 --enable-gpl --disable-stripping --enable-avresample --disable-filter=resample --enable-gnutls --enable-ladspa --enable-libaom --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --enable-libcodec2 --enable-libdav1d --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libgme --enable-libgsm --enable-libjack --enable-libmp3lame --enable-libmysofa --enable-libopenjpeg --enable-libopenmpt --enable-libopus --enable-libpulse --enable-librabbitmq --enable-librsvg --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libsrt --enable-libssh --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx265 --enable-libxml2 --enable-libxvid --enable-libzmq --enable-libzvbi --enable-lv2 --enable-omx --enable-openal --enable-opencl --enable-opengl --enable-sdl2 --enable-pocketsphinx --enable-libmfx --enable-libdc1394 --enable-libdrm --enable-libiec61883 --enable-chromaprint --enable-frei0r --enable-libx264 --enable-shared
  libavutil      56. 51.100 / 56. 51.100
  libavcodec     58. 91.100 / 58. 91.100
  libavformat    58. 45.100 / 58. 45.100
  libavdevice    58. 10.100 / 58. 10.100
  libavfilter     7. 85.100 /  7. 85.100
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  7.100 /  5.  7.100
  libswresample   3.  7.100 /  3.  7.100
  libpostproc    55.  7.100 / 55.  7.100
Input #0, image2, from '/mnt/etc/factorio-app/preprocess/Mk2/%08d.png':
  Duration: 00:00:24.84, start: 0.000000, bitrate: N/A
    Stream #0:0: Video: png, rgba(pc), 1920x1080 [SAR 3779:3779 DAR 16:9], 25 tbr, 25 tbn, 25 tbc
Stream mapping:
  Stream #0:0 -> #0:0 (png (native) -> vp9 (libvpx-vp9))
Press [q] to stop, [?] for help
[libvpx-vp9 @ 0x55d0a92cd200] v1.9.0
[libvpx-vp9 @ 0x55d0a92cd200] Neither bitrate nor constrained quality specified, using default CRF of 32
Output #0, webm, to '/mnt/etc/factorio-app/output/Mk2.webm':
  Metadata:
    encoder         : Lavf58.45.100
    Stream #0:0: Video: vp9 (libvpx-vp9), yuva420p, 1920x1080 [SAR 1:1 DAR 16:9], q=-1--1, 25 fps, 1k tbn, 25 tbc
    Metadata:
      encoder         : Lavc58.91.100 libvpx-vp9
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: N/A
frame=  619 fps=7.2 q=0.0 Lsize=   17575kB time=00:00:24.80 bitrate=5805.0kbits/s speed=0.288x    
video:17523kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.296984%
```


