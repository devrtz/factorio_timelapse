#!/bin/bash

INPUT_DIR="$1"
OUTPUT_DIR="$2"

if [ -z "${OUTPUT_DIR}" ]; then
   OUTPUT_DIR="output"
fi

PROJECT_NAME=$(basename "${INPUT_DIR}")

FFMPEG_FILE_FORMAT="%08d.png"
FFMPEG_OUT_FILE="${PROJECT_NAME}".webm

# test if folders exist
if [ -d "${INPUT_FOLDER}" ]; then
    echo Input folder "${INPUT_FOLDER}" is not a directory
    exit 1
fi

if [ ! -d "${OUTPUT_DIR}" ]; then
    echo Output folder "${OUTPUT_DIR}" is not a directory
    exit 1
fi

# all good if we reach here. let's fire up ffmeg to render us a timelapse \o/
IN_FILES="${INPUT_DIR}/${FFMPEG_FILE_FORMAT}"
OUT_FILE="${OUTPUT_DIR}/${FFMPEG_OUT_FILE}"

ffmpeg -y -i "${IN_FILES}" -vf fps=25 "${OUT_FILE}"
