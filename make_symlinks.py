#!/usr/bin/python3
import os
import sys
import glob


def usage ():
    text = f"""Usage: {sys.argv[0]} <input> [output]

This tool will create sequentially numbered symlinks
for use in f.e. a ffmpeg commandline invocation:
`ffmpeg -i "%08d.png" ...`

The directory `<input>` must exist and contain files.
If `[output]` is not specified the symlinks will be created
in a suitably named subfolder in `processed/`
"""
    print(text)

def check_matching_file_extensions(files):
    extension = None
    for fname in files:
        if extension is None:
            extension = os.path.splitext(fname)[-1]
        if not fname.endswith(extension):
            return None

    return extension

def clean_directory(dst, regex='*'):
    files = glob.glob(os.path.join(dst, regex))
    for f in files:
        os.unlink(f)

def make_symlinks (src, dst, regex='*'):
    files_in = sorted(os.listdir(src))
    if not os.path.exists(dst):
        os.mkdir(dst)
    if not os.path.isdir(dst):
        print(f"{dst} is not a directory. Aborting...")
        sys.exit(1)

    file_ext = check_matching_file_extensions(files_in)
    if file_ext is None:
        print(f"File extension for files in {dst} not matching. Aborting...")
        sys.exit(1)

    clean_directory(dst, regex)
    cnt = 0
    for fname in files_in:
        in_path = os.path.join(src, fname)
        out_path = os.path.join(dst, f"{cnt:08}{file_ext}")
        os.symlink (in_path, out_path)
        cnt += 1

    return cnt

def main ():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        usage()
        sys.exit(1)

    input_folder = os.path.abspath(sys.argv[1])
    project = os.path.basename(input_folder)

    if len(sys.argv) == 2:
        output_folder = os.path.join(os.getcwd(), "processed", project)

    if len(sys.argv) == 3:
        output_folder = os.path.abspath(sys.argv[2])


    if not os.path.exists(input_folder) or \
        not os.path.isdir(input_folder):
        print(f"Invalid input folder ({input_folder}) given")
        sys.exit(1)

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    if not os.path.isdir(output_folder):
        print(f"`{output_folder}` is not a directory! Aborting...")
        sys.exit(1)

    print(f"Creating symlinks for project {project}\n"
           f"Linking from {input_folder} to {output_folder}")

    n_files = make_symlinks (input_folder, output_folder, regex="*.png")

    print (f"Created {n_files} symlinks.")


if __name__ == '__main__':
    main()
